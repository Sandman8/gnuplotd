/**
    Copyright: Copyright (c) 2018- Alexander Orlov. All rights reserved.
    License: $(LINK2 https://opensource.org/licenses/MIT, MIT License).
    Authors: Alexander Orlov, $(LINK2 mailto:sascha.orlov@gmail.com, sascha.orlov@gmail.com) 
*/

/**
    A simplistic binding for gnuplot manipulation via pipes. 
    See http://ndevilla.free.fr/gnuplot/ for original, however not official C-bindings. 
    Based on 2.11 version. However, ignoring all windows features. 
*/

module gnuplotd; 

import std.process : pipeProcess, Redirect; 
import std.stdio : File;
import std.algorithm.iteration : each, filter; 
import core.stdc.limits : CHAR_BIT; 
import std.range; 
import std.path; 
debug
{
    import std.algorithm; 
}

/**
@brief    gnuplot session handle (opaque type).

This structure holds all necessary information to talk to a gnuplot
session. It is built and returned by start() and later used
by all functions in this module to communicate with the session, then
meant to be closed by finish().

This structure is meant to remain opaque, you normally do not need
to know what is contained in there.
*/
enum GP_MAX_TMP_FILES = size_t.sizeof * CHAR_BIT; 
private struct GnuPlotCtrl
{	
    /** Pipe to gnuplot process */
    typeof(pipeProcess("gnuplot", Redirect.stdin)) gnucmd; 

    /** Number of currently active plots */
    size_t nplots;

    /** Current plotting style */
    string pstyle = "points";

    /** Pointer to table of names of temporary files */
    string[GP_MAX_TMP_FILES] tmp_filename_tbl;
    
    /** Number of temporary files */
    size_t ntmp;
}

/**
@brief    Opens up a gnuplot session, ready to receive commands.
@return   Newly created gnuplot control structure.

This opens up a new gnuplot session, ready for input. The struct
controlling a gnuplot session should remain opaque and only be
accessed through the provided functions.

The session must be closed using finish().
*/
auto start()
{
	import std.process : ProcessException; 

	GnuPlotCtrl gpc; 

	try 
	{
		gpc.gnucmd = pipeProcess("gnuplot", Redirect.stdin); 
	}
	catch(ProcessException pe)
	{
		import core.stdc.stdlib : exit;
		import std.stdio : stderr;
		
		stderr.writeln("There was an error while connecting to gnuplot. Please check proper installation"); 
		stderr.writeln(pe.msg); 

		exit(1);
	}
	return gpc; 
}

/**
finalizer for gnuplot control object. returns the underlying stream 
*/
auto finish(ref GnuPlotCtrl gpc)
{
    import std.file : remove;    
    gpc.tmp_filename_tbl[].filter!(el => !el.empty).each!(s => s.remove); 
	return gpc.gnucmd; 
}

/**
general put function for the gnuplot controller structure
*/
void put(Args...)(ref GnuPlotCtrl gpc, string cmd, Args args)
{
	gpc.gnucmd.stdin.writefln(cmd, args); 
	gpc.gnucmd.stdin.flush; 
}

/**
style setting for the gnuplot controller structure.
*/
void setStyle(ref GnuPlotCtrl gpc, string plot_style)
{
	import core.stdc.string : strcmp, strcpy; 
	static immutable string[] knownStyles = 
	["lines", "points", "linespoints", "impulses", "dots", "steps", "errorbars", "boxes", "boxerrorbars"]; 
	import std.algorithm : canFind; 
	import core.stdc.stdio : fprintf;
	import std.stdio : stderr; 
	if(!knownStyles.canFind(plot_style))
    {
        stderr.writeln("warning: unknown requested style: using points") ;
        gpc.pstyle = "points";
    } else {
    	gpc.pstyle = plot_style; 
    }
}

/**
x label setting for gnuplot controller structure
*/
void setXlabel(ref GnuPlotCtrl gpc, string label)
{
    put(gpc, "set xlabel \"%s\"", label) ;
}

/**
y label setting for gnuplot controller structure
*/
void setYlabel(ref GnuPlotCtrl gpc, string label)
{
    put(gpc, "set ylabel \"%s\"", label) ;
}

/**
recreation (reusing) of gnuplot controller structure
*/
void resetPlot(ref GnuPlotCtrl gpc)
{
    gpc.finish; 
    gpc = start; 
}

/**
x plotting for gnuplot controller structure
*/
void plotX(Range)(ref GnuPlotCtrl gpc, Range darr, string title)
{
    if (!gpc.gnucmd.stdin.isOpen || darr.empty) return;

    /* Open temporary file for output   */
    string tmpfname = tmpFile(gpc);
    import std.stdio : File, stderr; 
    auto tmpfd = File(tmpfname, "w");
    
    if (!tmpfd.isOpen) {
        stderr.writeln("cannot create temporary file: exiting plot") ;
        return;
    }

    /* Write data to this file  */
    darr.each!(d => tmpfd.writefln("%.18e", d));

    //fclose(tmpfd) ;

    plotAtmpfile(gpc, tmpfname, title);
}

/**
xy plotting for gnuplot controller structure
*/
void plotXY(RangeX, RangeY)(ref GnuPlotCtrl gpc, RangeX xarr, RangeY yarr, string title)
{
    if (!gpc.gnucmd.stdin.isOpen || xarr.empty || yarr.empty) return;
    
    import std.stdio : File, stderr; 
    if(xarr.length != yarr.length)
    {
    	
    	stderr.writeln("x and y have different lengths");
    	return; 
    }

    /* Open temporary file for output   */
    string tmpfname = tmpFile(gpc);
    auto tmpfd = File(tmpfname, "w");

    if (!tmpfd.isOpen) {
        stderr.writeln("cannot create temporary file: exiting plot");
        return;
    }

    /* Write data to this file  */

    import std.range : iota; 
    iota(xarr.length).each!(i => tmpfd.writefln("%.18e %.18e", xarr[i], yarr[i]));

    //fclose(tmpfd) ;

    plotAtmpfile(gpc, tmpfname, title);
    return ;
}

/**
a common interface for a single plotting event for gnuplot controller structure
*/
void plotOnce(RangeX, RangeY)(string title, string style, string label_x, string label_y, RangeX x, RangeY y)
{
	auto gpc = start;

	if (x.empty) return;

	if (!gpc.gnucmd.stdin.isOpen) return ;

	if (!style.empty)
	{
	    setStyle(gpc, style);
	}
	else
	{
	    setStyle(gpc, "lines");
	}

	if (!label_x.empty)
	{
	    setXlabel(gpc, label_x);
	}
	else
	{
	    setXlabel(gpc, "X");
	}

	if (!label_y.empty)
	{
	    setYlabel(gpc, label_y);
	}
	else
	{
	    setYlabel(gpc, "Y");
	}

	if (y.empty)
	{
	    plotX(gpc, x, title);
	}
	else
	{
	    plotXY(gpc, x, y, title);
	}
	import std.stdio : writeln;
	writeln("press ENTER to continue");

    import core.stdc.stdio : getchar; 
	while (getchar()!='\n') {}
	  
	gpc.finish; 
}

/**
slope plotting for gnuplot controller structure
*/
void plotSlope(ref GnuPlotCtrl gpc, double a, double b, string title)
{
    string cmd = (gpc.nplots > 0) ? "replot" : "plot";
    
    title = title.empty ? "(none)" : title;

    put(gpc, "%s %.18e * x + %.18e title \"%s\" with %s", cmd, a, b, title, gpc.pstyle);

    gpc.nplots++;
}

/**
equation plotting for gnuplot controller structure
*/
void plotEquation(ref GnuPlotCtrl gpc, string equation, string title)
{
 	string cmd = gpc.nplots ? "replot" : "plot";
    title = title.empty ? "(none)" : title;

    put(gpc, "%s %s title \"%s\" with %s", cmd, equation, title, gpc.pstyle);
    gpc.nplots++;
}

private string tmpFile(ref GnuPlotCtrl gpc)
{
	import std.uuid : randomUUID; 
    import std.file : tempDir; 

    assert(gpc.tmp_filename_tbl[gpc.ntmp] is null);
    import std.stdio : File, stderr; 
    /* Open one more temporary file? */
    if (gpc.ntmp == GP_MAX_TMP_FILES - 1) {
        stderr.writefln("maximum # of temporary files reached (%d): cannot open more", GP_MAX_TMP_FILES);
        return string.init;
    }

    gpc.tmp_filename_tbl[gpc.ntmp] = tempDir ~ dirSeparator ~ randomUUID.toString;
    gpc.ntmp++;
    return gpc.tmp_filename_tbl[gpc.ntmp - 1];
}


private void plotAtmpfile(ref GnuPlotCtrl gpc, string tmp_filename, string title)
{
    string cmd = (gpc.nplots > 0) ? "replot" : "plot";
    title = (title.empty) ? "(none)" : title;
    put(gpc, "%s \"%s\" title \"%s\" with %s", cmd, tmp_filename, title, gpc.pstyle);
    gpc.nplots++;
}

/**
csv dumping for gnuplot controller structure (x var only)
*/
int writeXcsv(Range)(string fileName, Range darr, string title)
{
    if (fileName.empty || darr.empty)
    {
        return -1;
    }

    auto fileHandle = File(fileName, "w");

    if (!fileHandle.isOpen)
    {
        return -1;
    }

    // Write Comment.
    if (!title.empty)
    {
        fileHandle.writefln("# %s", title);
    }

    /* Write data to this file  */
    darr.each!((i, d) => fileHandle.writefln("%d, %.18e", i, d));

    return 0;
}

/**
csv dumping for gnuplot controller structure (xy vars)
*/
int writeXYcsv(RangeX, RangeY)(string fileName, RangeX xarr, RangeY yarr, string title)
{
    if (fileName.empty || xarr.empty || yarr.empty)
    {
        return -1;
    }

    auto fileHandle = File(fileName, "w");

    if (!fileHandle.isOpen)
    {
        return -1;
    }

    // Write Comment.
    if (!title.empty)
    {
        fileHandle.writefln("# %s", title);
    }

    /* Write data to this file  */
    iota(xarr.length).each!(i => fileHandle.writefln("%.18e, %.18e", xarr[i], yarr[i]));

    return 0;
}

/**
csv dumping for gnuplot controller structure (many columns)
*/
int writeMultiCsv(RoR)(string fileName, RoR xListPtr, string title)
{
    if (fileName.empty || xListPtr.empty || xListPtr.front.empty)
    {
        return -1;
    }

    auto fileHandle = File(fileName, "w");

    if (!fileHandle.isOpen)
    {
        return -1;
    }

    // Write Comment.
    if (!title.empty)
    {
        fileHandle.writefln("# %s", title);
    }

    /* Write data to this file  */
    
    for (auto j = 0; j < xListPtr.front.length; j++)
    {
        fileHandle.writef("%d, %.18e", j, xListPtr[0][j]);

    	for (auto i = 1; i < xListPtr.length; i++)    
        {
            fileHandle.writef(", %.18e", xListPtr[i][j]);
        }

        fileHandle.writeln;
    }

    return 0;
}

///
unittest
{
    auto gpc = start; 
    assert(gpc.gnucmd.stdin.isOpen); 
    gpc.finish; 
}

///
unittest
{
    import std.stdio : writeln; 
	auto gpc = gnuplotd.start; 
	assert(gpc.pstyle == "points");
	assert(gpc.gnucmd.stdin.isOpen); 
    gpc.put("set terminal gif animate"); // delay 100 (= 1 sec)
    gpc.put("set terminal gif animate");
    gpc.put("set output \"./tests/anim1.gif\"");
	double phase;
	
    writeln("*** example of gnuplot control through D ***");
    /*

    set terminal gif animate delay 100
    set output 'foobar.gif'
    stats 'datafile' nooutput
    set xrange [-0.5:1.5]
    set yrange [-0.5:5.5]

    do for [i=1:int(STATS_blocks)] {
        plot 'datafile' index (i-1) with circles
    }
    
    */
    for (phase = 0.1; phase < 10; phase += 0.1)
    {
        gpc.put("plot sin(x+%g)", phase);
    }
    
    for (phase = 10; phase >= 0.1; phase -= 0.1)
    {
        gpc.put("plot sin(x+%g)", phase);
    }
    gpc.finish; 
}

///
unittest
{
    import std.stdio : writeln; 
	import core.thread : Thread; 
	import core.time : dur; 
	enum SLEEP_LGTH = dur!("seconds")(2); 
	enum NPOINTS = 50; 
	auto gpc1 = start; 
	auto gpc2 = start; 
	auto gpc3 = start; 
	auto gpc4 = start; 

	double[NPOINTS] x;
    double[NPOINTS] y;
    int i;

    /*
     * Initialize the gnuplot handle
     */
    writeln("*** example of gnuplot control through D ***");

    /*
     * Slopes
     */
    gpc1.setStyle("lines") ;
    
    writeln("*** plotting slopes");
    writeln("y = x");
    gpc1.plotSlope(1.0, 0.0, "unity slope") ;
    Thread.sleep(SLEEP_LGTH) ;

    writeln("y = 2*x") ;
    gpc1.plotSlope(2.0, 0.0, "y=2x") ;
    Thread.sleep(SLEEP_LGTH) ;

    writeln("y = -x") ;
    gpc1.plotSlope(-1.0, 0.0, "y=-x") ;
    Thread.sleep(SLEEP_LGTH) ;

    
    /*
     * Equations
     */
    gpc1.resetPlot;
    writeln();
    writeln();
    writeln("*** various equations");
    writeln("y = sin(x)");
    gpc1.plotEquation("sin(x)", "sine");
    Thread.sleep(SLEEP_LGTH);

    writeln("y = log(x)");
    gpc1.plotEquation("log(x)", "logarithm");
    Thread.sleep(SLEEP_LGTH) ;

    writeln("y = sin(x)*cos(2*x)");
    gpc1.plotEquation("sin(x)*cos(2*x)", "sine product");
    Thread.sleep(SLEEP_LGTH) ;


    /*
     * Styles
     */
    gpc1.resetPlot;
    writeln(); 
    writeln();

    writeln("*** showing styles");

    writeln("sine in points");
    gpc1.setStyle("points");
    gpc1.plotEquation("sin(x)", "sine");
    Thread.sleep(SLEEP_LGTH) ;
    
    writeln("sine in impulses") ;
    gpc1.setStyle("impulses");
    gpc1.plotEquation("sin(x)", "sine");
    Thread.sleep(SLEEP_LGTH) ;
    
    writeln("sine in steps");
    gpc1.setStyle("steps");
    gpc1.plotEquation("sin(x)", "sine");
    Thread.sleep(SLEEP_LGTH) ;

    /*
     * User defined 1d and 2d point sets
     */
    gpc1.resetPlot;
    gpc1.setStyle("impulses");
    writeln(); 
    writeln(); 

    writeln("*** user-defined lists of doubles");
    for (i=0 ; i<NPOINTS ; i++) {
        x[i] = cast(double)i*i ;
    }
    gpc1.plotX(x, "user-defined doubles");
    Thread.sleep(SLEEP_LGTH) ;

	writeln("*** user-defined lists of points");
    for (i=0 ; i<NPOINTS ; i++) {
        x[i] = cast(double)i ;
        y[i] = cast(double)i * cast(double)i ;
    }
    gpc1.resetPlot;
    gpc1.setStyle("points");
    gpc1.plotXY(x, y, "user-defined points");
    Thread.sleep(SLEEP_LGTH);


    /*
     * Multiple output screens
     */

    writeln();
    writeln();

    writeln("*** multiple output windows");
    gpc1.resetPlot;
    gpc1.setStyle("lines");
    gpc2.setStyle("lines");
    gpc3.setStyle("lines");
    gpc4.setStyle("lines");

    writeln("window 1: sin(x)");
    gpc1.plotEquation("sin(x)", "sin(x)");
    Thread.sleep(SLEEP_LGTH) ;
    writeln("window 2: x*sin(x)");
    gpc2.plotEquation("x*sin(x)", "x*sin(x)");
    Thread.sleep(SLEEP_LGTH) ;
    writeln("window 3: log(x)/x");
    gpc3.plotEquation("log(x)/x", "log(x)/x");
    Thread.sleep(SLEEP_LGTH) ;
    writeln("window 4: sin(x)/x");
    gpc4.plotEquation("sin(x)/x", "sin(x)/x");
    Thread.sleep(SLEEP_LGTH) ;
    
    gpc1.finish; 
    gpc2.finish; 
    gpc3.finish; 
    gpc4.finish; 
    writeln(); 
    writeln();
    writeln("*** end of gnuplot example");
}
/+
///
unittest
{
	auto gpc = gnuplotd.start; 
	assert(gpc.pstyle == "points");
	assert(gpc.gnucmd.stdin.isOpen); 
	gpc.put("set terminal png");
	gpc.put("set output \"./tests/sine.png\"");
	gpc.plotEquation("sin(x)", "Sine wave");
    gpc.finish; 
}

///
unittest
{
	writeXcsv("./tests/testfile_x.csv", [1.1, 1.2, 1.3], "test1, test2");
	writeXYcsv("./tests/testfile_xy.csv", [1.1, 1.2, 1.3], [2.1, 2.2, 2.3], "test1, test2");
	
	double[][] multi; 
	multi.length = 3; 
	import std.algorithm : each; 
	multi.each!((ref c) => c.length = 5); 

	multi[0][0] = 0.0;
	multi[1][0] = 1.0; 
	multi[2][0] = 2.0; 
	multi[0][1] = 3.0; 
	multi[1][1] = 4.0; 
	multi[2][1] = 5.0; 
	multi[0][2] = 6.0; 
	multi[1][2] = 7.0; 
	multi[2][2] = 8.0; 
	multi[0][3] = 9.0; 
	multi[1][3] = 10.0; 
	multi[2][3] = 11.0; 
	multi[0][4] = 12.0; 
	multi[1][4] = 13.0; 
	multi[2][4] = 14.0; 
	//writeln(multi);
	writeMultiCsv("./tests/testfile_multi.csv", multi, "test1, test2");
}

///
unittest
{
    import std.stdio : writeln; 
    auto gpc = gnuplotd.start;
    assert(gpc.pstyle == "points");
    assert(gpc.gnucmd.stdin.isOpen);
    gnuplotd.put(gpc, "set terminal png");
    gnuplotd.put(gpc, "set output \"./tests/test.png\"");

    enum NPOINTS = 50; 
    double[NPOINTS] x;
    double[NPOINTS] y;

    writeln("*** user-defined lists of points");
    for (auto i=0 ; i<NPOINTS ; i++) {
        x[i] = cast(double)i ;
        y[i] = cast(double)i * cast(double)i ;
    }

    
    gpc.plotXY(x, y, "user-defined points");
    const(double)[] xConst = x[]; 
    const(double)[] yConst = y[]; 
    gpc.plotXY(xConst, yConst, "user-defined points");
    gpc.finish; 
}
+/